import os
import glob
import argparse

import cv2
import numpy as np

def exprange(start, end, step=1):
    for i in range(start, end, step):
        yield 0x1 << i

def quantify_image(image, k, type='kmeans'):

    # Transform colored image into an feature column shape (w*h, 3, 1)
    img_array = np.reshape(image, (-1, 3)).astype(np.float32)

    compactness, labels, centers = cv2.kmeans(
        data=img_array,
        K=k,
        bestLabels=None,
        criteria=(cv2.TERM_CRITERIA_MAX_ITER, 10),
        attempts=10,
        flags=cv2.KMEANS_RANDOM_CENTERS
    )

    img_quant = centers[labels]
    img_quant = np.reshape(img_quant, image.shape).astype(np.uint8)
    return img_quant

def main(output_dir, assets_dir, ext, show):
    type = 'kmeans'
    out_path = lambda path: os.path.join(output_dir, f"{path}.{ext}")
    img_fps = glob.glob(os.path.join(assets_dir, "*"))

    for img_fp in img_fps:
        img_name = os.path.basename(img_fp).split('.')[0]
        img = cv2.imread(img_fp, cv2.IMREAD_COLOR)

        for k in exprange(2, 8):
            img_quant = quantify_image(img, k, type=type)
            cv2.imwrite(out_path(f"{img_name}-{type}-{k}"), img_quant)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-o", "--output_dir", required=False, default='bin')
    parser.add_argument("-a", "--assets_dir", required=False, default='assets')
    parser.add_argument("--ext", required=False, default='png')
    parser.add_argument("--show", action='store_true')

    args = parser.parse_args()
    kwargs = vars(args)

    try:
        main(**kwargs)
    finally:
        cv2.destroyAllWindows()
