# Native libs
import os
import glob
import time
import argparse

# Third-party libs
import cv2
import numpy as np

# Custom libs
from kernels import h1, h2, h2_sep, h3, h4

def visual_dft(img, shape=None):

    if not shape:
        shape = img.shape

    # Compute discrete fourier transform for image and return output with
    # specified shape and type complex128
    dft_img = np.fft.fft2(img, s=shape)

    # Shift zero frequency to image center
    dft_img = np.fft.fftshift(dft_img)

    # Scale the magnitude of complex matrix using log for visualization
    dft_vis = np.log(np.abs(dft_img) + 1)

    # Normalize between 0 and 255 and cast to uint8 for visualization
    dft_vis = (dft_vis-np.min(dft_vis))/(np.max(dft_vis)-np.min(dft_vis))
    dft_vis = (dft_vis*255).astype(np.uint8)

    return dft_vis

def dft_gaussian(img, ksize=5, sigma=1.0):

    # Get a 5x5 by gaussian kernel with specified sigma
    kernel = cv2.getGaussianKernel(ksize=ksize, sigma=sigma)
    kernel = kernel*kernel.T

    # Compute discrete fourier transform for image and kernel
    dft_img = np.fft.fft2(img)
    dft_kernel = np.fft.fft2(kernel, img.shape)

    # Translate zero frequency to image center
    dft_img = np.fft.fftshift(dft_img)
    dft_kernel = np.fft.fftshift(dft_kernel)

    # Multiply both images in frequency domain
    dft_img = dft_img * dft_kernel

    # Shift zero frequency component back to image corner in order to call ifft
    dft_img = np.fft.ifftshift(dft_img)

    # Compute the inverse fourier transform to go back to spatial domain
    out = np.fft.ifft2(dft_img)
    out = out.real.astype(np.uint8)

    return out

def main(assets_dir, output_dir):

    # Create output dir in case it doesn't exist
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    # Define kernel size and sigma ranges for tests
    k_range = range(5, 13, 2)
    sigma_range = range(1, 13, 4)

    # Visualize kernels in frequency domain
    dft_h1 = visual_dft(h1, shape=(256, 256))
    dft_h2 = visual_dft(h2, shape=(256, 256))
    dft_h3 = visual_dft(h3, shape=(256, 256))
    dft_h4 = visual_dft(h4, shape=(256, 256))

    cv2.imwrite(os.path.join(output_dir, f'fourier-h1.png'), dft_h1)
    cv2.imwrite(os.path.join(output_dir, f'fourier-h2.png'), dft_h2)
    cv2.imwrite(os.path.join(output_dir, f'fourier-h3.png'), dft_h3)
    cv2.imwrite(os.path.join(output_dir, f'fourier-h4.png'), dft_h4)

    # Save all Gaussian kernels frequency domain visualizations
    for k in k_range:
        for sigma in sigma_range:
            kernel = cv2.getGaussianKernel(ksize=k, sigma=sigma)
            kernel = kernel*kernel.T
            dft_kernel = visual_dft(kernel, shape=(256, 256))
            cv2.imwrite(os.path.join(output_dir, f'fourier-k{k}-sigma{sigma}.png'), dft_kernel)

    img_fns = glob.glob(os.path.join(assets_dir, '*'))
    for fn in img_fns:

        # Get image name
        img_name = os.path.basename(fn)

        # Read image as numpy array
        img = cv2.imread(fn, cv2.IMREAD_GRAYSCALE)

        # Save frequency domain visualization
        dft_img = visual_dft(img)
        cv2.imwrite(os.path.join(output_dir, f'fourier-{img_name}'), dft_img)

        # 1.1 Filtering in the spatial domain
        # Apply all four filters individually
        img_h1 = cv2.filter2D(img, -1, h1, borderType=cv2.BORDER_REPLICATE)
        img_h2 = cv2.filter2D(img, -1, h2, borderType=cv2.BORDER_REPLICATE)
        img_h2_sep = cv2.sepFilter2D(img, -1, h2_sep, h2_sep, borderType=cv2.BORDER_REPLICATE)
        img_h3 = cv2.filter2D(img, -1, h3, borderType=cv2.BORDER_REPLICATE)
        img_h4 = cv2.filter2D(img, -1, h4, borderType=cv2.BORDER_REPLICATE)

        cv2.imwrite(os.path.join(output_dir, f'h1-{img_name}'), img_h1)
        cv2.imwrite(os.path.join(output_dir, f'h2-{img_name}'), img_h2)
        cv2.imwrite(os.path.join(output_dir, f'h3-{img_name}'), img_h3)
        cv2.imwrite(os.path.join(output_dir, f'h4-{img_name}'), img_h4)

        # Combine filters h3 and h4
        img_h3 = img_h3.astype(np.float32)
        img_h4 = img_h4.astype(np.float32)
        img_mag = np.sqrt(img_h3**2 + img_h4**2)
        img_map = np.clip(img_mag, 0, 255).astype(np.uint8)
        cv2.imwrite(os.path.join(output_dir, f'mag-{img_name}'), img_mag)

        # 1.2 Filtering in the frequency domain
        for k in k_range:
            for sigma in sigma_range:
                img_gauss = dft_gaussian(img, k, sigma)
                cv2.imwrite(os.path.join(output_dir, f'k{k}-sigma{sigma}-{img_name}'), img_gauss)
                gauss_dft = visual_dft(img_gauss)
                cv2.imwrite(os.path.join(output_dir, f'fourier-k{k}-sigma{sigma}-{img_name}'), gauss_dft)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-o", "--output_dir", required=False, default='bin')
    parser.add_argument("-a", "--assets_dir", required=False, default='assets')
    args = parser.parse_args()
    kwargs = vars(args)

    try:
        main(**kwargs)
    finally:
        cv2.destroyAllWindows()
