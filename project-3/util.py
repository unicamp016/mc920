import numpy as np

kernel_1 = np.ones(shape=(1, 100), dtype=np.uint8)
kernel_2 = np.ones(shape=(200, 1), dtype=np.uint8)
kernel_3 = np.ones(shape=(1, 30), dtype=np.uint8)

word_kernel = np.ones(shape=(30, 1), dtype=np.uint8)

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

class Rectangle:
    id = 0
    def __init__(self, x1, y1, x2, y2):
        self.id = Rectangle.id
        self.p1 = Point(x1, y1)
        self.p2 = Point(x2, y2)
        self.area = (x2-x1+1)*(y2-y1+1)
        Rectangle.id += 1

        assert self.area >= 0

    def translate(self, point):
        self.p1.x += point.x
        self.p2.x += point.x
        self.p1.y += point.y
        self.p2.y += point.y

    def compute_stats(self, img):
        rows, cols = img.shape

        self.whites = 0
        self.vert_trans = 0
        self.hori_trans = 0
        for i in range(self.p1.y, self.p2.y+1):
            for j in range(self.p1.x, self.p2.x+1):
                if img[i,j] == 255:
                    self.whites += 1
                else:
                    # Black to white since img is inverted
                    if i+1 < rows and img[i+1, j] == 255:
                        self.vert_trans += 1
                    if j+1 < cols and img[i, j+1] == 255:
                        self.hori_trans += 1
        self.desc1 = self.whites/self.area
        self.desc2 = float('inf')
        if self.whites > 0:
            self.desc2 = (self.vert_trans + self.hori_trans)/self.whites
            
