import os
import glob
import argparse

import cv2
import skimage.measure as skm
import matplotlib.pyplot as plt

from kernels import *

def scale(img, min, max):
    """
    Scale an image to range [min, max], casting the array to float to ensure
    no overflow when scaling.
    """
    img = img.astype(np.float32)
    return (img - np.min(img))*(max - min)/(np.max(img) - np.min(img)) + min

def dither(img, dither_mask, keep_dims=True):
    """
    Perform dithering on img using the given dither_mask. Here we chose to
    round the values after scaling the image in order to achieve a better
    distribution in the extremes. We also opted to invert the image
    intensities before normalizing in order to fill the dither_mask in
    increasing order of its indices. If we implemented the according
    to the project specs, the order in which we would fill the black pixels
    would be inverted.
    """
    # Original shape
    h, w = img.shape

    # Normalize image to dither mask dimensions range
    norm = scale(255-img, 0, dither_mask.shape[0]*dither_mask.shape[1])

    # Rounding has better results than truncating
    norm = np.round(norm)

    # Allocate our expanded halftone matrix
    hft_shape = img.shape[0]*dither_mask.shape[0], img.shape[1]*dither_mask.shape[1]
    hft = np.zeros(hft_shape, dtype=np.uint8)

    # Assign binary pixel values according to dither mask
    di, dj = dither_mask.shape
    blacks = np.zeros(dither_mask.shape)
    whites = np.ones(dither_mask.shape)*255
    for i in range(0, hft.shape[0], dither_mask.shape[0]):
        for j in range(0, hft.shape[1], dither_mask.shape[1]):
            hft[i:i+di, j:j+dj] = \
                np.where(
                    norm[i//di, j//dj] <= dither_mask,
                    whites,
                    blacks
                )

    # Resize image to original input size
    if keep_dims:
        hft = cv2.resize(hft, dsize=(w, h), fx=0, fy=0, interpolation=cv2.INTER_CUBIC)
    return hft

def traverse(shape, order):
    """
    Generator to yield indices for different types of traversals through
    a matrix.
    """
    if order == 'horizontal':
        for i in range(shape[0]):
            for j in range(shape[1]):
                yield i, j, 1
    elif order == 'alternating':
        for i in range(0, shape[0], 2):
            for j in range(0, shape[1]):
                yield i, j, 1
            if i + 1 < shape[0]:
                for j in reversed(range(0, shape[1])):
                    yield i+1, j, -1
    else:
        raise NotImplementedError

def error_diffusion(img, order):
    """
    Apply Floyd-Steinberg's error diffusion algorithm using the specified
    image traversal.
    """
    rows, cols = img.shape
    img = img.astype(np.float32)    # Creates a copy of the image
    for i, j, ord in traverse(img.shape, order):

        # Compute error and set current pixel value
        if img[i,j] > 128:
            error = img[i,j]-255
            img[i,j] = 255
        else:
            error = img[i,j]
            img[i,j] = 0

        # Propagate error if pixels are within bounds
        if j+ord < cols and j+ord >= 0:
            img[i, j+ord] += 0.4375*error
        if i+1 < rows:
            img[i+1, j] += 0.3125*error
            if j-ord >= 0 and j-ord < cols:
                img[i+1, j-ord] += 0.1875*error
            if j+ord < cols and j+ord >= 0:
                img[i+1, j+ord] += 0.0625*error

    return img.astype(np.uint8)

def main(assets_dir, output_dir, ext):
    img_fns = glob.glob(os.path.join(assets_dir, '*'))
    dither_masks = [dither_1, dither_bayer]

    # Write ssim scores to file
    f = open(os.path.join(output_dir, 'floyd-steinberg.txt'), 'w')
    f.write('SSIM Scores:\n')

    for img_fn in img_fns:
        # 1.1: Ordered dithering
        img_name = os.path.basename(img_fn).split('.')[0]
        img = cv2.imread(img_fn, cv2.IMREAD_GRAYSCALE)
        for i, dm in enumerate(dither_masks):
            hft = dither(img, dither_mask=dm, keep_dims=False)
            hft_fn = os.path.join(output_dir, f'{img_name}-ht-{i+1}.{ext}')
            cv2.imwrite(hft_fn, hft, params=[cv2.IMWRITE_PXM_BINARY])

        # 1.2: Floyd-Steinberg error diffusion technique
        edh = error_diffusion(img, order='horizontal')
        eda = error_diffusion(img, order='alternating')

        edh_ssim = skm.compare_ssim(img, edh)
        eda_ssim = skm.compare_ssim(img, eda)
        f.write(
            f'{img_name}:\n'\
            f'\thorizontal: {edh_ssim:.4f}\n'\
            f'\talternating: {eda_ssim:.4f}\n'\
        )
        f.flush()

        edh_fn = os.path.join(output_dir, f'{img_name}-ed-horizontal.{ext}')
        eda_fn = os.path.join(output_dir, f'{img_name}-ed-alternating.{ext}')
        cv2.imwrite(edh_fn, edh, params=[cv2.IMWRITE_PXM_BINARY])
        cv2.imwrite(eda_fn, eda, params=[cv2.IMWRITE_PXM_BINARY])

    f.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-o", "--output_dir", required=False, default='bin')
    parser.add_argument("-a", "--assets_dir", required=False, default='assets')
    parser.add_argument("-e", "--ext", required=False, default='pbm')

    args = parser.parse_args()
    kwargs = vars(args)

    try:
        main(**kwargs)
    finally:
        cv2.destroyAllWindows()
