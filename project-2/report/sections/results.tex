\section{Resultados e Discussão}
  A seguir serão apresentados os resultados dos algoritmos de pontilhado ordenado e de difusão de erro, bem como uma análise da aplicação dos mesmos.

  \subsection{Pontilhado Ordenado}
    Os resultados da aplicação do algoritmo de pontilhado ordenado são dado pelas Figuras~\ref{fig:baboon-dithering} a~\ref{fig:retina-dithering}. A Figura~\ref{fig:baboon-dithering} mostra como a aplicação da técnica torna a diferença entre a imagem binária e a original quase imperceptível. Usando uma máscara $3 \times 3$, como na Figura~\ref{fig:baboon-ht-3}, é possível perceber alguns rastros da quantização, principalmente no nariz, onde as variações suaves entre os níveis de cinza na imagem original parecem rachaduras na imagem binarizada. Esse efeito é muito menos acentuados na Figura~\ref{fig:baboon-ht-4}, onde a quantização é feita com 17 níveis de cinza ao invés de 10 e não há rachaduras devido à alta resolução da imagem.

    Na Figura~\ref{fig:monarch-dithering}, os efeitos da quantização são bem mais nítidos devido às variações suaves nos níveis de cinza do fundo da imagem original. Note como nas Figuras~\ref{fig:monarch-ht-3} e~\ref{fig:monarch-ht-4}, os fundos parecem estar dividos em camadas com cores distintas, ao invés de haver uma transição suave entre as intensidades. Na Figura~\ref{fig:retina-dithering}, o resultado é extremamente satisfatório, havendo poquíssimas perdas nos detalhes da imagem.

    Em geral, é de se esperar que matrizes maiores terão resultados melhores na aplicação do algoritmo de pontilhado ordenado, já que a resolução da imagem aumenta proporcionalmente à matriz e a quantização da imagem binária também aumenta.

    % Talvez comentar do uso de memória

  \begin{figure*}[htbp]
    \centering
    \quad
    \subfloat[]{
      \label{fig:baboon}
      \includegraphics[width=.3\textwidth]{./images/baboon.png}
    }
    \quad
    \subfloat[]{
      \label{fig:baboon-ht-3}
      \includegraphics[width=.3\textwidth]{./images/baboon-ht-1.png}
    }
    \quad
    \subfloat[]{
      \label{fig:baboon-ht-4}
      \includegraphics[width=.3\textwidth]{./images/baboon-ht-2.png}
    }
    \caption{Imagem do babuíno original~\protect\subref{fig:baboon}, após aplicação do algoritmo de pontilhado ordenado seguindo a ordenação dada pela matriz $3 \times 3$ da Equação~\ref{eq:dither3}~\protect\subref{fig:baboon-ht-3} e a matriz $4 \times 4$ dada pela Equação~\ref{eq:dither4}~\protect\subref{fig:baboon-ht-4}.}.
    \label{fig:baboon-dithering}
  \end{figure*}

  \begin{figure*}[htbp]
    \centering
    \quad
    \subfloat[]{
      \label{fig:monarch}
      \includegraphics[width=.3\textwidth]{./images/monarch.png}
    }
    \quad
    \subfloat[]{
      \label{fig:monarch-ht-3}
      \includegraphics[width=.3\textwidth]{./images/monarch-ht-1.png}
    }
    \quad
    \subfloat[]{
      \label{fig:monarch-ht-4}
      \includegraphics[width=.3\textwidth]{./images/monarch-ht-2.png}
    }
    \caption{Imagem da borboleta monarca original~\protect\subref{fig:monarch}, após aplicação do algoritmo de pontilhado ordenado seguindo a ordenação dada pela matriz $3 \times 3$ da Equação~\ref{eq:dither3}~\protect\subref{fig:monarch-ht-3} e a matriz $4 \times 4$ dada pela Equação~\ref{eq:dither4}~\protect\subref{fig:monarch-ht-4}.}.
    \label{fig:monarch-dithering}
  \end{figure*}

  \begin{figure*}[htbp]
    \centering
    \quad
    \subfloat[]{
      \label{fig:retina}
      \includegraphics[width=.3\textwidth]{./images/retina.png}
    }
    \quad
    \subfloat[]{
      \label{fig:retina-ht-3}
      \includegraphics[width=.3\textwidth]{./images/retina-ht-1.png}
    }
    \quad
    \subfloat[]{
      \label{fig:retina-ht-4}
      \includegraphics[width=.3\textwidth]{./images/retina-ht-2.png}
    }
    \caption{Imagem original~\protect\subref{fig:retina} de uma retina, após aplicação do algoritmo de pontilhado ordenado seguindo a ordenação dada pela matriz $3 \times 3$ da Equação~\ref{eq:dither3}~\protect\subref{fig:retina-ht-3} e a matriz $4 \times 4$ dada pela Equação~\ref{eq:dither4}~\protect\subref{fig:retina-ht-4}.}.
    \label{fig:retina-dithering}
  \end{figure*}

  \subsection{Pontilhado com Difusão de Erro}
    Técnicas de pontilhado com difusão de erro procuram distribuir a diferença entre o valor exato de cada pixel e seu valor aproximado a um conjunto de pixels adjacentes. O algoritmo de Floyd-Steinberg usa a distribuição de erro dada pela Figura~\ref{fig:floyd-steinberg}~\cite{pedrini2008pdi}.

    A ordem em que a imagem é percorrida pode produzir resultados diferentes no processo de meio-tom. Um dos problemas de usar uma máscara pequena como a de Floyd-Steinberg e percorrer a imagem sempre no mesmo sentido é a introdução de artefatos estruturantes na imagem resultante. Esses artefatos tornam-se mais nítidos em superfícies homogêneas, como no fundo da Figura~\ref{fig:monarch-horizontal}, onde os pontos brancos resultantes do acúmulo de erro aparecem em uma mesma coluna na saída. Isso nos mostra um outro problema desse método, que é a introdução de ruído sal e pimenta na imagem de saída. Por exemplo, na Figura~\ref{fig:retina-horizontal} e~\ref{fig:retina-alternating}, há inúmeros pixels brancos no centro da retina onde deveria ser preto, justamente porque o erro é carregado ao percorrer a imagem e eventualmente muitos pixels escuros acabam acumulando erro suficente para gerar um pixel branco.

    Na varredura alternada, os pixels ruidosos resultantes do acúmulo de erro ocorrem tipicamente em colunas intercaladas na imagem de saída. Isso reduz os artefatos estruturantes na imagem de saída, tornando sua aparência mais natural. Isso fica nítido na Figura~\ref{fig:monarch-alternating}, onde vemos que os pixels ruidosos ocorrem em colunas alternadas.

    De forma quantitativa, podemos avaliar a qualidade aparente das imagens meio-tom através da métrica SSIM, dada pela Equação~\ref{eq:ssim}. Essa métrica nos dá valores no intervalo $[-1, 1]$, onde um valor de -1 ocorre para imagens inversamente correlacionadas e 1 para imagens totalmente correlacionadas (imagens iguais)~\cite{wang2004ssim}. A Tabela~\ref{tab:ssim} mostra o resultado da métrica SSIM computada entre a imagem original e as imagens após a aplicação do algoritmo de difusão de erro percorrendo a imagem diretamente e de forma alternada. Note como o valor da métrica é extremamente baixo para todas as imagens, como era de se esperar, considerando que a imagem binária só assume os valores 0 e 255, enquanto a imagem original pode assumir todos os 256 níveis de cinza. De forma quantitativa, é difícil dizer que uma métrica é melhor que a outra, já que percorrendo a imagem diretamente nos dá melhores resultados para as Figuras~\ref{fig:baboon} e~\ref{fig:monarch}, mas nos dá piores resultados para a Figura~\ref{fig:retina}. De qualquer forma, a diferença no valor da métrica é insignificativa, tornando a questão dos artefatos estruturantes muito mais relevante para decisão de qual método devemos aplicar.

    \begin{equation}\label{eq:ssim}
        SSIM(x, y) = \frac{(2\mu_x\mu_y + c_1) (2\sigma_{xy} + c_2)}{(\mu_x^2 \mu _y^2 + c_1) (\sigma_x^2 + \sigma_y^2 + c_2)}
    \end{equation}

    \begin{table}
      \centering
      \caption{Valores da métrica SSIM entre a imagem original e as imagens após a aplicação do algoritmo de Floyd-Steinberg percorrendo os pixels da esquerda para direita (diretamente) e de forma alternada}\label{tab:ssim}
      \begin{tabular}{ |c|c|c| }
       \hline
       \textbf{Imagem} & \textbf{Direta} & \textbf{Alternada} \\\hline
       Baboon & 0,0959 & 0,0937 \\
       Borboleta & 0,0615 & 0,0602 \\
       Retina & 0,0709 & 0,0716 \\
       \hline
      \end{tabular}
    \end{table}
    Na prática, a técnica de pontilhado com difusão de erros é muito empregada pois oferece uma binarização extremamente fiel à imagem original sem alterar a sua resolução espacial.

  \begin{figure*}[htbp]
    \centering
    \quad
    \subfloat[]{
      \label{fig:baboon}
      \includegraphics[width=.3\textwidth]{./images/baboon.png}
    }
    \quad
    \subfloat[]{
      \label{fig:baboon-horizontal}
      \includegraphics[width=.3\textwidth]{./images/baboon-ed-horizontal.png}
    }
    \quad
    \subfloat[]{
      \label{fig:baboon-alternating}
      \includegraphics[width=.3\textwidth]{./images/baboon-ed-alternating.png}
    }
    \caption{Imagem do babuíno original~\protect\subref{fig:baboon}, após aplicação do algoritmo de pontilhado com difusão de erro de Floyd-Steinberg, percorrendo os pixels da esquerda para direita~\protect\subref{fig:baboon-horizontal} e alternando entre esquerda para direita e direita para esquerda ~\protect\subref{fig:baboon-alternating}.}.
    \label{fig:baboon-error-diffusion}
  \end{figure*}

  \begin{figure*}[htbp]
    \centering
    \quad
    \subfloat[]{
      \label{fig:monarch}
      \includegraphics[width=.3\textwidth]{./images/monarch.png}
    }
    \quad
    \subfloat[]{
      \label{fig:monarch-horizontal}
      \includegraphics[width=.3\textwidth]{./images/monarch-ed-horizontal.png}
    }
    \quad
    \subfloat[]{
      \label{fig:monarch-alternating}
      \includegraphics[width=.3\textwidth]{./images/monarch-ed-alternating.png}
    }
    \caption{Imagem da borboleta monarca original~\protect\subref{fig:monarch}, após aplicação do algoritmo de pontilhado com difusão de erro de Floyd-Steinberg, percorrendo os pixels da esquerda para direita~\protect\subref{fig:monarch-horizontal} e alternando entre esquerda para direita e direita para esquerda ~\protect\subref{fig:monarch-alternating}.}.
    \label{fig:monarch-error-diffusion}
  \end{figure*}

  \begin{figure*}[htbp]
    \centering
    \quad
    \subfloat[]{
      \label{fig:retina}
      \includegraphics[width=.3\textwidth]{./images/retina.png}
    }
    \quad
    \subfloat[]{
      \label{fig:retina-horizontal}
      \includegraphics[width=.3\textwidth]{./images/retina-ed-horizontal.png}
    }
    \quad
    \subfloat[]{
      \label{fig:retina-alternating}
      \includegraphics[width=.3\textwidth]{./images/retina-ed-alternating.png}
    }
    \caption{Imagem original~\protect\subref{fig:retina} da retina, após aplicação do algoritmo de pontilhado com difusão de erro de Floyd-Steinberg, percorrendo os pixels da esquerda para direita~\protect\subref{fig:retina-horizontal} e alternando entre esquerda para direita e direita para esquerda ~\protect\subref{fig:retina-alternating}.}.
    \label{fig:retina-error-diffusion}
  \end{figure*}
