\section{Implementação e Discussão}
  O algoritmo de agrupamento \emph{k-means} pode ser aplicado às imagens através de uma única chamada para a função \texttt{cv2.kmeans}, que retorna uma lista de rótulos para cada pixel, bem como um dicionário com o centroide correspondente a cada rótulo. O algoritmo inicia escolhendo aleatoriamente $k$ centroides dentre os pixels da imagem, onde um pixel é uma 3-upla BGR. Em seguida, calcula-se a distância de todos os pixels para o centroide, rotulando o pixel com o índice do centroide cuja distância euclideana para ele é mínima. Para cada agrupamento, calcula-se a média de todos os pixels e define-se as médias dos $k$ agrupamentos como os novos centroides. Esse procedimento é repetido até que o critério de parada seja satisfeito, que foi definido por um número máximo de 10 iterações. A cada iteração, o algoritmo busca minimizar a compacidade $C$ dos centroides encontrados, que é definida pela Equação~\ref{eq:compactness}, onde $k$ é número de agrupamentos, $m_k$ é o número de pontos no agrupamento $k$, $p_{ij}$ é o pixel $j$ pertencente ao agrupamento $i$ e $c_i$ é o centroide do grupo $i$.

  \begin{equation}\label{eq:compactness}
    C = \sum_{i = 0}^{k} \sum_{j = 0}^{m_k} {||p_{ij} - c_{i} ||}_2
  \end{equation}

  Em cada chamada para \texttt{cv2.kmeans}, o algoritmo descrito acima é aplicado 10 vezes e o conjunto de centroides que obteve a menor compacidade $C$ é retornado. Essa repetição é importante pois dependendo dos pontos iniciais escolhidos, a solução obtida pode ser muito pior que a solução ótima.

  A Figura~\ref{fig:peppers-kmeans} mostra o resultado da aplicação do algoritmo \emph{k-means} à imagem dos pimentões da Figura~\ref{fig:peppers}. Note que para $k=8$ a imagem já é bem próxima da original e as diferenças de maior destaque são os artefatos de sombra que aparecem no pimentão verde e o vermelho à esquerda do amarelo. Porém, é interessante notar que a imagem não é descaracterizada e todos os objetos são facilmente identificados. Essa quantificação mais agressiva para 8 ou 16 cores pode ser usada como uma espécie de filtro que dá um efeito de cartoonização à imagem. A partir de $k=64$, a diferença entre a imagem quantizada e a original é quase imperceptível. Esse resultado só é possível porque a imagem original apresenta uma pequena variedade de cores, mas mostra que o \emph{k-means} é uma alternativa interessante para compressão de imagens com perdas, pois explora a redundância psicovisual. Ao armazenar a imagem, seria necessário guardar apenas o conjunto de centróides e os rótulos de cada pixel, onde a redundância de codificação pode ser explorada.

  \begin{figure*}[!htbp]
    \centering
    \quad
    \subfloat[Original]{
      \label{fig:kpeppers}
      \includegraphics[width=.3\textwidth]{./images/peppers.png}
    }
    \quad
    \subfloat[$k=8$]{
      \label{fig:peppers-8}
      \includegraphics[width=.3\textwidth]{./images/peppers-kmeans-8.png}
    }
    \quad
    \subfloat[$k=16$]{
      \label{fig:peppers-16}
      \includegraphics[width=.3\textwidth]{./images/peppers-kmeans-16.png}
    } \\
    \quad
    \subfloat[$k=32$]{
      \label{fig:peppers-32}
      \includegraphics[width=.3\textwidth]{./images/peppers-kmeans-32.png}
    }
    \quad
    \subfloat[$k=64$]{
      \label{fig:peppers-64}
      \includegraphics[width=.3\textwidth]{./images/peppers-kmeans-64.png}
    }
    \quad
    \subfloat[$k=128$]{
      \label{fig:peppers-128}
      \includegraphics[width=.3\textwidth]{./images/peppers-kmeans-128.png}
    }
    \caption{Algoritmo de agrupamento \emph{k-means} aplicado à imagem dos pimentões~\protect\subref{fig:kpeppers} para quantizar o número de cores na imagem para $k=\{8, 16, 32, 64, 128\}$.}
    \label{fig:peppers-kmeans}
  \end{figure*}

  Na Figura~\ref{fig:watch-kmeans}, temos o resultado da aplicação do \emph{k-means} à imagem da Figura~\ref{fig:watch}. Note como para todos os valores de $k$ maiores que 8, a imagem nunca é descaracterizada e os detalhes da imagem original são facilmente identificados. Na Figura~\ref{fig:watch-8}, o eixo verde à esquerda do relógio torna-se preto e praticamente desaparece na sobra do livro. Para todas as imagens quantizadas, ao contrário da Figura~\ref{fig:peppers-kmeans}, os artefatos de sombra são perceptíveis, principalmente a sombra sob o livro na parte superior da imagem. Note como até para $k=128$, uma espécie de arco-íris de níveis de cinza é formada. Em geral, os efeitos da quantização são mais perceptíveis nas imagens com transições suaves de cores.

  \begin{figure*}[!htbp]
    \centering
    \quad
    \subfloat[Original]{
      \label{fig:kwatch}
      \includegraphics[width=.3\textwidth]{./images/watch.png}
    }
    \quad
    \subfloat[$k=8$]{
      \label{fig:watch-8}
      \includegraphics[width=.3\textwidth]{./images/watch-kmeans-8.png}
    }
    \quad
    \subfloat[$k=16$]{
      \label{fig:watch-16}
      \includegraphics[width=.3\textwidth]{./images/watch-kmeans-16.png}
    } \\
    \quad
    \subfloat[$k=32$]{
      \label{fig:watch-32}
      \includegraphics[width=.3\textwidth]{./images/watch-kmeans-32.png}
    }
    \quad
    \subfloat[$k=64$]{
      \label{fig:watch-64}
      \includegraphics[width=.3\textwidth]{./images/watch-kmeans-64.png}
    }
    \quad
    \subfloat[$k=128$]{
      \label{fig:watch-128}
      \includegraphics[width=.3\textwidth]{./images/watch-kmeans-128.png}
    }
    \caption{Algoritmo de agrupamento \emph{k-means} aplicado na imagem do relógio~\protect\subref{fig:kwatch} para quantizar o número de cores na imagem para $k=\{8, 16, 32, 64, 128\}$.}
    \label{fig:watch-kmeans}
  \end{figure*}
