\section{Implementação e Discussão}
  Nesta seção, os passos para implementar o algoritmo de segmentação de texto serão detalhados e os resultados obtidos em cada etapa do algoritmo serão discutidos.

  \subsection{Fechamento}
  Antes de aplicar qualquer operador morfológico, foi necessário inverter a imagem, de forma que o primeiro plano seja branco e o fundo preto. Isso é necessário por causa da implementação dos operadores no \texttt{OpenCV}. O resultado da inversão da entrada da Figura~\ref{fig:input} é dado pela Figura~\ref{fig:inverted}.

  \begin{figure}[htbp]
    \centering
    \includegraphics[width=.8\textwidth]{images/bitmap-inverted.png}
    \caption{Imagem de entrada invertida}\label{fig:inverted}
  \end{figure}

  Em seguida, aplicamos os elementos estruturantes da Figura~\ref{fig:kernels} em operações de fechamento, que consiste na dilatação seguida pela erosão. O resultado da aplicação dos elementos da Figura~\ref{fig:kernels} em operações de fechamento na imagem invertida é dado pela Figura~\ref{fig:closing}.

  Na Figura~\ref{fig:k1}, nota-se que o uso de um elemento estruturante no formato de uma linha horizontal no fechamento preenche os vãos entre os caracteres e as palavras do texto, destacando as fileiras da imagem que têm um objeto. A Figura~\ref{fig:k2} mostra o resultado da aplicação de um elemento vertical na operação de fechamento, que destaca as colunas na imagem que possuem qualquer objeto. A intersecção das duas imagens resultantes, dada pela Figura~\ref{fig:and}, nos dá uma boa aproximação para os trechos da imagem que correspondem aos objetos, preenchendo os caracteres das linhas com texto. Finalmente, a Figura~\ref{fig:k3}, mostra o resultado do fechamento usando um elemento estruturante vertical de dimensão menor, que ajuda a preencher as colunas muito finas do fundo entre os objetos do primeiro plano.

  \begin{figure}
    \centering
    \begin{tikzpicture}[
        cell/.style={rectangle,draw=black},
        space/.style={minimum height=0.5em, matrix of nodes,row sep=-\pgflinewidth,
        column sep=-\pgflinewidth,
        column 1/.style={font=\ttfamily}},
        text depth=0.5ex,
        text height=1em,
        nodes in empty cells
      ]

      \matrix (first) [
        space,
        column 1/.style={font=\ttfamily},
        column 2/.style={nodes={cell,minimum width=1em}}
      ]
      {
        1   & \Lbullet \\
        2   & \Lbullet \\
        :   & \Lbullet \\
        200 & \Lbullet \\
      };

      \matrix (second) [
        right=of first, space,
        row 1/.style={nodes={cell,minimum width=2.2em}},
        row 2/.style={font=\ttfamily},
        row 4/.style={nodes={cell,minimum width=2.2em}},
        row 5/.style={font=\ttfamily}
      ]{
        \Lbullet   & \Lbullet  & \Lbullet & \Lbullet \\
        1 & 2 & .. & 100 \\
        & & & \\
        \Lbullet   & \Lbullet  & \Lbullet & \Lbullet \\
        1 & 2 & .. & 30 \\
      };

    \end{tikzpicture}
    \caption{Elementos estruturantes usados nas operações de fechamento. À esquerda temos o segundo kernel aplicado de dimensões $200 \times 1$, à direita e acima temos o primeiro de dimensões $1 \times 100$ e embaixo o terceiro com dimensões $1 \times 30$.}\label{fig:kernels}
  \end{figure}

  \begin{figure}[htbp]
    \centering
    \quad
    \subfloat[]{
      \label{fig:k1}
      \includegraphics[width=.42\textwidth]{./images/bitmap-k1.png}
    }
    \quad
    \subfloat[]{
      \label{fig:k2}
      \includegraphics[width=.42\textwidth]{./images/bitmap-k2.png}
    } \\
    \quad
    \subfloat[]{
      \label{fig:and}
      \includegraphics[width=.8\textwidth]{./images/bitmap-and.png}
    } \\
    \quad
    \subfloat[]{
      \label{fig:k3}
      \includegraphics[width=.8\textwidth]{./images/bitmap-k3.png}
    } \\
    \caption{Imagem invertida da Figura~\ref{fig:inverted} após a aplicação do elemento estruturante $1 \times 100$~\protect\subref{fig:k1} e após a aplicação do elemento estruturante $200 \times 1$~\protect\subref{fig:k2} em operações de fechamento. Em seguida, aplica-se a intersecção entre as duas imagens resultantes~\protect\subref{fig:and}. Finalmente, é feito um fechamento com um elemento estruturante de dimensões $1 \times 30$ na intersecção~\protect\subref{fig:k3}.}
    \label{fig:closing}
  \end{figure}

  \subsection{Componentes Conexas}\label{sec:connected-components}
  A próxima etapa do algoritmo envolve a identificação das componentes conexas na imagem da Figura~\ref{fig:k3}. Para rotular as componentes, foi feita uma chamada para a função \texttt{cv2.connectedComponents}, considerando-se como critério de conectividade a vizinhança-8 de cada pixel. Usando a imagem rotulada, calculou-se os retângulos envolventes de cada componente, guardando a coluna e a fileira mínima e máxima e associando-as ao canto superior esquerdo e canto inferior direito do retângulo, respectivamente. Os retângulos envolventes encontrados para as componentes conexas da Figura~\ref{fig:k3} é dado pela Figura~\ref{fig:labels-all}.

  Detectadas as componentes, o próximo passo é classificá-las em texto e não-texto. Para isso, foram usados dois descritores. O primeiro descritor $d_1$ é computado a partir da razão entre o número de pixels brancos e o número total de pixels dentro do retângulo envolvente. No caso, usamos o número de pixels brancos porque estamos trabalhando com a imagem invertida. O descritor $d_2$ é dado pela razão entre o número de transições verticais e horizontais de preto para branco e o número de pixels brancos no retângulo. Ao passar pelos pixels pretos, verificamos se o pixel abaixo ou à direita são brancos, incrementando o contador de transições. Se o retângulo não possui pixels brancos, temos que $d_2 = \infty$.

  Avaliando o valor dos descritores para as regiões destacadas na Figura~\ref{fig:labels-all}, optou-se por classificar como texto as regiões em que $0,2 < d_1 < 0,5$ e $0.2 < d_2 < 0,4$. Removendo as componentes que não satisfazem essas inequações, obteve-se o resultado da Figura~\ref{fig:labels-text} com somente as componentes de texto destacadas. Um valor muito maior que 0,5 para $d_1$ significa que mais de 50\% do retângulo é coberto pelo objeto, que é tipicamente muito mais do que temos nas regiões de texto, onde há inúmeros vãos entre os caracteres. Um valor abaixo de 0,2 para $d_1$ também não é razoável para texto, já que isso só acontece para regiões que quase não possuem pixels do objeto e para a maioria dos caracteres espera-se que eles ocupem pelo menos 20\% do retângulo envolvente. O descritor $d_2$ vai ter um valor baixo para regiões homogêneas de objetos e um valor intermediário para texto, onde aproximadamente metade dos pixels brancos têm um pixel de transição correspondente.

  \begin{figure}[htbp]
    \centering
    \quad
    \subfloat[]{
      \label{fig:labels-all}
      \includegraphics[width=.8\textwidth]{./images/bitmap-labels.png}
    } \\
    \quad
    \subfloat[]{
      \label{fig:labels-text}
      \includegraphics[width=.8\textwidth]{./images/bitmap-labels-text.png}
    }
    \caption{Retângulos envolventes para todas as componentes conexas detectadas a partir da Figura~\ref{fig:k3}~\protect\subref{fig:labels-all}. Em seguida, removeu-se todas as componentes não-texto usando o critério de descritores~\protect\subref{fig:labels-text}.}
    \label{fig:labels}
  \end{figure}

  \subsection{Detecção de Palavras}
  Para detectar os blocos de palavras, foram utilizados operadores morfológicos nas regiões classificadas como texto na etapa anterior. O algoritmo consiste em dilatar 3 vezes a região de texto usando um elemento estruturante $3 \times 3$ e depois aplicar o fechamento usando um elemento de dimensão $30 \times 1$. As dilatações sucessivas têm o propósito de engrossar os caracteres do texto, juntando os caracteres em blocos de palavra, como pode ser visto na Figura~\ref{fig:word-dilate}. A operação de fechamento, cujo resultado é dado pela Figura~\ref{fig:word-close}, busca eliminar os pontilhados pretos nos blocos de palavras, que podem acabar gerando componentes conexas espúrias. Finalmente, o mesmo método da Seção~\ref{sec:connected-components} é usado para detectar as componentes conexas no trecho de texto e cada uma é considerada como um bloco de palavra. O resultado da segmentação das palavras da Figura~\ref{fig:word-orig} é dado pela Figura~\ref{fig:word-labels}.

  \begin{figure}[htbp]
    \centering
    \quad
    \subfloat[]{
      \label{fig:word-orig}
      \includegraphics[width=.8\textwidth]{./images/bitmap-word.png}
    } \\
    \quad
    \subfloat[]{
      \label{fig:word-dilate}
      \includegraphics[width=.8\textwidth]{./images/bitmap-word-dilate.png}
    } \\
    \quad
    \subfloat[]{
      \label{fig:word-close}
      \includegraphics[width=.8\textwidth]{./images/bitmap-word-close.png}
    } \\
    \quad
    \subfloat[]{
      \label{fig:word-labels}
      \includegraphics[width=.8\textwidth]{./images/bitmap-word-labels.png}
    }
    \caption{Etapas para a detecção de blocos de palavras em um trecho de texto~\protect\subref{fig:word-orig}. Primeiro aplica-se a dilatação 3 vezes com um elemento estruturante $3 \times 3$~\protect\subref{fig:word-dilate}. Em seguida, usamos o operador de fechamento com elemento $30 \times 1$~\protect\subref{fig:word-close}. Finalmente, detecta-se as componentes conexas da região para então calcular os retângulos envolventes~\protect\subref{fig:word-labels}.}
    \label{fig:word}
  \end{figure}

  Aplicando esse método a todas as componentes de texto detectadas na etapa anterior, obteve-se o resultado final dado pela Figura~\ref{fig:words}, onde foram detectadas \textbf{39 linhas de texto} e \textbf{241 blocos de palavras}. Note como à natureza do método de agrupar caracteres próximos, a detecção de palavras inclui em muitos dos casos acentuação e qualquer outro caractere que esteja agregado. Mesmo assim, o resultado ainda é satisfatório, já que sempre que há um caractere de espaço, considera-se um novo bloco de palavra. Uma das falhas do algoritmo foi na detecção do número vértice 3 no grafo da Figura~\ref{fig:words}. Esse erro foi propagado desde a aplicação dos operadores morfológicos iniciais, que acabaram conectando as arestas do grafo ao número do vértice.

  \begin{figure}[htbp]
    \centering
    \includegraphics[width=.8\textwidth]{images/bitmap-words.png}
    \caption{Imagem de entrada após a segmentação dos blocos de palavras}\label{fig:words}
  \end{figure}
