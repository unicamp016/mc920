import os
import glob
import argparse
import subprocess

import cv2

from util import *

def draw_rects(img, rects, show_id=False):
    img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    for rect in rects:
        p1 = (rect.p1.x, rect.p1.y)
        p2 = (rect.p2.x, rect.p2.y)
        img = cv2.rectangle(img, p1, p2, color=(0, 0, 255), thickness=2)
        if show_id:
            img = cv2.putText(img,
                text=str(rect.id),
                org=(rect.p1.x+5, rect.p2.y-5),
                fontFace=cv2.FONT_HERSHEY_SIMPLEX,
                fontScale=0.7,
                thickness=2,
                color=(0, 0, 255)
            )
    return img

def find_word_rects(img, text, save_fn=None, ext='pbm'):
    text_img = img[text.p1.y:text.p2.y, text.p1.x:text.p2.x]
    text_img = text_img.copy() # Make sure we're not working with a numpy view

    dilate_img = cv2.dilate(text_img, kernel=np.ones((3,3)), iterations=4)
    close_img = cv2.morphologyEx(dilate_img, kernel=word_kernel, op=cv2.MORPH_CLOSE)
    count, labels = cv2.connectedComponents(close_img, connectivity=8)
    words = bounding_rects(labels, count)

    if save_fn:
        # Draw bounding boxes on word rectangles
        img_word = draw_rects(text_img, words)

        # Save all intermediate images
        cv2.imwrite(f'{save_fn}.{ext}', text_img)
        cv2.imwrite(f'{save_fn}-dilate.{ext}', dilate_img)
        cv2.imwrite(f'{save_fn}-close.{ext}', close_img)
        cv2.imwrite(f'{save_fn}-labels.png', img_word)

    # Translate origin to be relative to entire image
    for w in words:
        w.translate(text.p1)

    return words

def filter_text(rects):
    texts = []
    for rect in rects:
        if rect.desc1 > 0.2 and rect.desc1 < 0.5 and \
           rect.desc2 < 0.4 and rect.desc2 > 0.2:
            texts.append(rect)
    return texts

def bounding_rects(labels, count):
    rows, cols = labels.shape

    max_int = max(rows, cols)
    x1s = np.ones(shape=count-1, dtype=np.uint8) * max_int
    y1s = np.ones(shape=count-1, dtype=np.uint8) * max_int
    x2s = np.ones(shape=count-1, dtype=np.uint8) * (-1)
    y2s = np.ones(shape=count-1, dtype=np.uint8) * (-1)

    for i in range(rows):
        for j in range(cols):
            index = labels[i,j] - 1
            if index >= 0:
                x1s[index] = min(x1s[index], j)
                y1s[index] = min(y1s[index], i)
                x2s[index] = max(x2s[index], j)
                y2s[index] = max(y2s[index], i)

    rects = [Rectangle(x1s[i], y1s[i], x2s[i], y2s[i]) for i in range(count-1)]
    return rects

def main(assets_dir, output_dir, ext):

    img_fns = glob.glob(os.path.join(assets_dir, '*'))
    save_path = lambda fn: os.path.join(output_dir, fn)

    for img_fn in img_fns:
        img_name = os.path.basename(img_fn).split('.')[0]
        img = cv2.imread(img_fn, cv2.IMREAD_GRAYSCALE)
        cv2.imwrite(save_path(f'{img_name}.{ext}'), img)

        # Invert image to make white the foreground
        img = ~img
        cv2.imwrite(save_path(f'{img_name}-inverted.{ext}'), img)

        # Steps 1 and 2
        res_1 = cv2.morphologyEx(img, kernel=kernel_1, op=cv2.MORPH_CLOSE)
        cv2.imwrite(save_path(f'{img_name}-k1.{ext}'), res_1)

        # Steps 3 and 4
        res_2 = cv2.morphologyEx(img, kernel=kernel_2, op=cv2.MORPH_CLOSE)
        cv2.imwrite(save_path(f'{img_name}-k2.{ext}'), res_2)

        # Step 5
        res = res_1 & res_2
        cv2.imwrite(save_path(f'{img_name}-and.{ext}'), res)

        # Step 6
        res = cv2.morphologyEx(res, kernel=kernel_3, op=cv2.MORPH_CLOSE)
        cv2.imwrite(save_path(f'{img_name}-k3.{ext}'), res)

        # Find connected components using 8-neighborhood connectivity
        count, labels = cv2.connectedComponents(res, connectivity=8)

        # Compute the bounding rectangles for each label
        rects = bounding_rects(labels, count)
        img_rects = draw_rects(img, rects, show_id=True)
        cv2.imwrite(save_path(f'{img_name}-labels.png'), img_rects)

        # Determine necessary stats in order to compute descriptors
        for rect in rects:
            rect.compute_stats(img)

        texts = filter_text(rects)
        print(f"Found a total of {len(texts)} pieces of text.")
        img_text = draw_rects(img, texts)
        cv2.imwrite(save_path(f'{img_name}-labels-text.png'), img_text)

        words = []
        for text in texts:
            words.extend(find_word_rects(img, text))
        print(f"Found a total of {len(words)} blocks of words.")
        img_words = draw_rects(~img, words)
        cv2.imwrite(save_path(f'{img_name}-words.png'), img_words)

        # Show the process of segmenting a single piece of text
        find_word_rects(img, texts[0], save_fn=save_path(f'{img_name}-word'), ext=ext)



if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-o", "--output_dir", required=False, default='bin')
    parser.add_argument("-a", "--assets_dir", required=False, default='assets')
    parser.add_argument("--ext", required=False, default='pbm')

    args = parser.parse_args()
    kwargs = vars(args)

    try:
        main(**kwargs)
    finally:
        cv2.destroyAllWindows()
