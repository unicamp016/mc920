\section{Implementação e Discussão}
  Nesta seção, os passos para implementar o algoritmo de costuramento serão detalhados e os resultados obtidos em cada etapa serão discutidos.

  \subsection{Detecção e Descrição dos Pontos de Interesse}
    Antes de aplicar o algoritmo de detecção de pontos de interesse, as imagens coloridas são convertidas para imagens de níveis de cinza através de uma chamada para \texttt{cv2.cvtColor}. Foram feitos experimentos com dois algoritmos de detecção de pontos, o SIFT~\cite{lowe2004sift} e o ORB~\cite{rublee2011orb}.

    \subsubsection{SIFT}
      O SIFT (\emph{Scale Invariante Feature Transform}) usa pirâmides de diferenças de Gaussianas (DoG) para estimar o Laplaciano do Gaussiano da imagem em diferentes escalas. Os mínimos e máximos locais em uma vizinhança-26 na pirâmide são considerados como candidatos para pontos de interesse e a escala onde o ponto foi encontrado também é armazenada. Em seguida, o algoritmo usa a expansão de Taylor de segunda ordem para descartar pontos com pouco contraste. Para eliminar as bordas, o algoritmo usa a matriz Hessiana de segunda ordem e um limiar de bordas para manter apenas os pontos correspondentes a cantos na imagem. Finalmente, uma orientação predominante é designada para o ponto através da construção de um histograma de orientações na vizinhança do mesmo e a janela em torno do ponto é alinhada para essa orientação, que é subtraída de todas as demais na janela.

      Os passos de detecção dos pontos garantem que eles sejam invariantes a escala, rotação e translação. O fato de estarmos buscando pontos de interesse na imagem toda garante a invariância a translação. A invariância a escala vem do fato que computamos o descritor na imagem com a mesma escala em que o ponto foi encontrado na pirâmide. O ponto também é invariante a rotação porque rotacionamos a janela para alinhar-se à orientação predominante da vizinhança. O descritor é computado através de um conjunto de histogramas de orientação em vizinhanças $4 \times 4$ com 8 bins cada, onde os histogramas são construídos a partir dos valores de magnitude e orientação das amostras em uma vizinhança $16 \times 16$ ao redor do ponto de interesse. Como temos 16 vizinhanças $4 \times 4$ na região $16 \times 16$ com 8 bins cada, o descritor é formado por um vetor de $16 \cdot 8 = 128$ elementos. Para aumentar a invariância à iluminação, o descritor é normalizado para ter norma unitária.

      Na implementação, criou-se um detector através de uma chamada para \texttt{cv2.xfeatures2d.SIFT\_create}, limitando-o a encontrar apenas os 200 melhores pontos, ranqueados através da pontuação de Harris~\cite{harris1988corner}. Os demais argumentos do algoritmo não foram alterados e os valores default do OpenCV foram mantidos, que refletem os valores indicados por Lowe em seu paper~\cite{lowe2004sift}. O resultado da detecção dos pontos de interesse é dado pela Figura~\ref{fig:sift-keypoints}

      \begin{figure}[htbp]
        \centering
        \quad
        \subfloat[]{
          \label{fig:sift-1-keypoints}
          \includegraphics[width=.9\textwidth]{./images/photo1-sift-keypoints.jpg}
        } \\
        \quad
        \subfloat[]{
          \label{fig:sift-5-keypoints}
          \includegraphics[width=.9\textwidth]{./images/photo5-sift-keypoints.jpg}
        } \\
        \caption{Pares de imagens com os 200 melhores pontos de interesse de cada imagem detectados pelo SIFT usando os parâmetros default do OpenCV. No caso da imagem de satélite da plantação~\protect\subref{fig:sift-5-keypoints}, o algoritmo detectou apenas 27 e 54 pontos de interesse nas imagens da esquerda e direita, respectivamente.}
        \label{fig:sift-keypoints}
      \end{figure}

    \subsection{ORB}

    O ORB~\cite{rublee2011orb} (\emph{Oriented} FAST and \emph{Rotated} BRIEF) é uma alternativa rápida para o SIFT, já que usa o detector de pontos FAST~\cite{rosten2006fast} (\emph{Features From Accelerated Segments Test}) e o descritor BRIEF~\cite{calonder2010brief} (\emph{Binary Robust Independent Elementary Features}). O FAST é um detector invariante a escala e translação. Para tornar o ORB invariante a rotação, computa-se a orientação em uma janela em torno do ponto através do cálculo do vetor que liga o ponto em si ao centróide da vizinhança ponderado pela intensidade dos pixels. O descritor BRIEF consiste de uma string de bits, onde cada bit é o resultado de uma comparação entre intensidades de pixel em um par de localizações selecionado cuidadosamente. Dada a string de bits de tamanho $n$ e uma matriz $2 \times n$ onde cada coluna representa um ponto $(x, y)$ usado para comparação, a orientação obtida na detecção do ponto é usada para calcular a matriz de rotação e computar uma nova string de bits rotacionada pelo mesmo ângulo da orientação, obtendo-se um descritor invariante a rotação. A escolha de representar o descritor como uma string de bits torna-o muito mais barato de armazenar do que os descritores do SIFT. Os descritores SIFT são representados como floats e ocupam $128 \cdot 4 = 512$ bytes, enquanto o ORB usa 256 bits para descrever cada ponto, que equivale a 32 bytes.

    O resultado dos 200 melhores pontos de cada imagem detectados usando o ORB com os argumentos default do OpenCV e ranqueados através da pontuação de Harris é dado pela Figura~\ref{fig:orb-keypoints}.

    \begin{figure}[htbp]
      \centering
      \quad
      \subfloat[]{
        \label{fig:orb-1-keypoints}
        \includegraphics[width=.9\textwidth]{./images/photo1-orb-keypoints.jpg}
      } \\
      \quad
      \subfloat[]{
        \label{fig:orb-5-keypoints}
        \includegraphics[width=.9\textwidth]{./images/photo5-orb-keypoints.jpg}
      } \\
      \caption{Pares de imagens com os 200 melhores pontos de interesse de cada imagem detectados pelo ORB usando os parâmetros default do OpenCV. No caso da imagem de satélite da plantação~\protect\subref{fig:orb-5-keypoints}, o algoritmo detectou apenas 108 e 106 pontos de interesse nas imagens da esquerda e direita, respectivamente.}
      \label{fig:orb-keypoints}
    \end{figure}

  \subsection{Correspondências}
    O próximo passo no procedimento consiste em encontrar as corresondências entre os pontos de interesse no par de imagens e filtrá-las de acordo com algum critério. Para os pontos detectados com o SIFT, usou-se a norma $L^2$ da diferença entre os dois vetores de descritores para medir a distância entre os dois pontos de interesse~\cite{lowe2004sift}, dada pela Equação~\ref{eq:norm-l2}, onde $\mathbf{x}$ e $\mathbf{y}$ são descitores SIFT de pontos pertencentes a imagens distintas.

    \begin{equation}\label{eq:norm-l2}
      {||\mathbf{x} - \mathbf{y}||}_2 = \sqrt{\sum_{k=1}^{n} (x_k-y_k)^2}
    \end{equation}

    Para os descritores ORB, o cálculo da distância entre dois pontos de interesse é feito através da distância de Hamming, que é dada pela Equação~\ref{eq:hamming}, onde $\oplus$ é o operador lógico XOR, e os elementos $x_k$ e $y_k$ do descritor são bits. O uso da distância de Hamming para computar a distância de uma correspondência e a redução no consumo de memória são dois fatores que tornam o ORB um algoritmo muito mais econômico em espaço e tempo.

    \begin{equation}\label{eq:hamming}
      H(\mathbf{x}, \mathbf{y}) = \sum_{k=1}^{n} (x_k \oplus y_k)
    \end{equation}

    Dado que sabemos computar as distâncias das correspondências, basta definir um critério para selecionar as melhores. Usando um pareador brute-force, instanciado através de uma chamada para \texttt{cv2.BFMatcher}, bem como a flag \texttt{crossCheck} setada, foram selecionadas apenas as correspondências entre os pontos $p = (x_p, y_p)$ e $q = (x_q, y_q)$ onde o ponto $p$ é o melhor ponto dentre os pontos de interesse da imagem à esquerda para o ponto $q$ e o ponto $q$ é o melhor ponto de interesse dentre os da imagem à direita para o ponto $p$. Nesse critério, o melhor ponto é definido como o ponto cuja correspondência tem a menor distância dentre as demais. As correspondências encontradas entre os pontos detectados pelo SIFT é dada pela Figura~\ref{fig:sift-matches} e pelo ORB é dada pela Figura~\ref{fig:orb-matches}.

    % Discutir resultado

    \begin{figure}[htbp]
      \centering
      \quad
      \subfloat[]{
        \label{fig:sift-1-matches}
        \includegraphics[width=.9\textwidth]{./images/photo1-sift-all-matches.jpg}
      } \\
      \quad
      \subfloat[]{
        \label{fig:sift-5-matches}
        \includegraphics[width=.9\textwidth]{./images/photo5-sift-all-matches.jpg}
      } \\
      \caption{Todas as correspondências (vermelho) obtidas entre o par de imagens usando os pontos de interesse detectados (verde) pelo SIFT e o critério de que os pontos devem ser mutuamente as melhores opções um para o outro.}
      \label{fig:sift-matches}
    \end{figure}

    \begin{figure}[htbp]
      \centering
      \quad
      \subfloat[]{
        \label{fig:orb-1-matches}
        \includegraphics[width=.9\textwidth]{./images/photo1-orb-all-matches.jpg}
      } \\
      \quad
      \subfloat[]{
        \label{fig:orb-5-matches}
        \includegraphics[width=.9\textwidth]{./images/photo5-orb-all-matches.jpg}
      } \\
      \caption{Todas as correspondências (vermelho) obtidas entre o par de imagens usando os pontos de interesse detectados (verde) pelo ORB e o critério de que os pontos devem ser mutuamente as melhores opções um para o outro.}
      \label{fig:orb-matches}
    \end{figure}

    Como para encontrar a matriz de homografia são necessários apenas 4 correspondências, podemos descartar uma grande parte das correspondências que apresentam baixa correlação entre os pontos. Visando eliminar correspondências como as da Figura~\ref{fig:sift-1-matches}, onde o topo da torre na imagem à esquerda é pareado com uma janela no prédio da imagem à direita e na Figura~\ref{fig:orb-1-matches} em que partes da estátua são pareadas com pontos no topo do prédio, optou-se por selecionar apenas as 20 melhores correspondências, onde novamente uma distância menor entre os descritores significa que a correspondência é melhor. O resultado desse critério é dado pelas Figuras~\ref{fig:sift-only-best-matches} e~\ref{fig:orb-only-best-matches} para os descritores SIFT e ORB, respectivamente. Note como a grande maioria das correspondências errôneas foram eliminadas e só permaneceram as que pertencem à intersecção entre as imagens.

    \begin{figure}[htbp]
      \centering
      \quad
      \subfloat[]{
        \label{fig:sift-1-only-best-matches}
        \includegraphics[width=.9\textwidth]{./images/photo1-sift-only-best-matches.jpg}
      } \\
      \quad
      \subfloat[]{
        \label{fig:sift-5-only-best-matches}
        \includegraphics[width=.9\textwidth]{./images/photo5-sift-only-best-matches.jpg}
      } \\
      \caption{Vinte melhores correspondências (vermelho) obtidas entre o par de imagens obtidas pelo SIFT após o pareamento força-bruta.}
      \label{fig:sift-only-best-matches}
    \end{figure}

    \begin{figure}[htbp]
      \centering
      \quad
      \subfloat[]{
        \label{fig:orb-1-only-best-matches}
        \includegraphics[width=.9\textwidth]{./images/photo1-orb-only-best-matches.jpg}
      } \\
      \quad
      \subfloat[]{
        \label{fig:orb-5-only-best-matches}
        \includegraphics[width=.9\textwidth]{./images/photo5-orb-only-best-matches.jpg}
      } \\
      \caption{Vinte melhores correspondências (vermelho) obtidas entre o par de imagens obtidas pelo ORB após o pareamento força-bruta.}
      \label{fig:orb-only-best-matches}
    \end{figure}

  \subsection{Alinhamento}
    Para alinhar as imagens, é preciso primeiro encontrar a matriz de homografia. Como desejamos levar a imagem da direita para o espaço de coordenadas da imagem à esquerda, é feita uma chamada para a função \texttt{cv2.findHomography} usando os pontos de interesse de cada correspondência pertencentes à imagem da direita como argumento para o parâmetro \texttt{srcPoints} e os da imagem esquerda como os \texttt{dstPoints}. Para encontrar a matriz de homografia, precisamos resolver o sistema da Equação~\ref{perspective-transform}, onde $H$ é a matriz de homografia dada pela Equação~\ref{homography} e $\lambda$ é o fator de escala que mantém o valor da coordenada homogênea no espaço de coordenadas destino normalizada.

    \begin{gather}\label{perspective-transform}
      \lambda
      \begin{bmatrix}
        x' \\ y' \\ 1
      \end{bmatrix}
      = H
      \begin{bmatrix}
        x \\ y \\ 1
      \end{bmatrix}
    \end{gather}

    \begin{gather}\label{homography}
      H =
      \begin{bmatrix}
        a & b & c \\
        d & e & f \\
        i & j & 1 \\
      \end{bmatrix}
    \end{gather}

    O método utilizado para estimar a matriz de homografia $H$ é o RANSAC (\emph{Random Sample Consensus}). Primeiro são selecionadas 4 correspondências aleatórias dentre as 20 melhores (mínimo para estimar a matriz), a partir dos quais estima-se uma matriz de homografia $H'$. Para as correspondências remanescentes, verificamos se uma correspondência entre um ponto $\mathbf{s} = (x_s, y_s, 1)$ na imagem fonte e $\mathbf{d} = (x_d, y_d, 1)$ na imagem destino é um outlier a partir da inequação dada pela Equação~\ref{ransac-threshold}, onde usamos $\epsilon = 3$. Dada a matriz de homografia estimada $H'$ e os pontos considerados inliers, computamos o erro total $\delta$ da estimativa usando apenas os inliers através da Equação~\ref{back-projection-error}. A matriz de homografia com o menor erro é armazenada e esse processo é repetido 2000 vezes, onde no final retorna-se a melhor estimativa encontrada.

    \begin{equation}\label{ransac-threshold}
      {|| \mathbf{d} - H \cdot \mathbf{s} ||}_1 > \epsilon
    \end{equation}

    \begin{equation}\label{back-projection-error}
      \delta = \sum_i (x'_i - \frac{a'x_i + b'y_i + c'}{i'x_i + j'y_i + 1})^2 + (y'_i - \frac{d'x_i + e'y_i + f'}{i'x_i + j'y_i + 1})^2
    \end{equation}

    Dada a matriz de homografia, basta alinhar a imagem direita com a esquerda, que pode ser feito através de uma chamada para \texttt{cv2.warpPerspective}. Essa função apenas aplica a matriz $H$ encontrada a todos os pixels da imagem direita, interpolando as cores RGB na imagem destino usando o método de interpolação bilinear e preenchendo os pixels extrapolados com 0s. O tamanho da imagem de saída é dado pela soma das larguras do par de imagens e a altura máxima entre o par, formando uma imagem longa capaz de guardar o resultado de uma translação para direita da mesma ordem da largura da imagem. Note que se a imagem da direita tiver uma translação vertical em relação à da esquerda, ela não aparecerá no resultado final. O resultado da aplicação transformação projetiva é dado pelas Figuras~\ref{fig:sift-warped} e~\ref{fig:orb-warped} para os algoritmos SIFT e ORB, respectivamente.

    Na Figura~\ref{fig:sift-1-warped}, note como a suposição de só haver uma translação para direita é em geral válida, mas pequenos trechos nos cantos superior dirieto e inferior esquerdo são perdidos. Comparando com a Figura~\ref{fig:orb-1-warped}, é possível observar uma divergência nos resultados, onde o ORB resultou em uma alteração de escala, enquanto o SIFT manteve a escala original e apenas rotacionou e transladou a imagem da direita.

    Já nas Figuras~\ref{fig:sift-5-warped} e~\ref{fig:orb-5-warped}, o SIFT claramente encontrou correspondências errôneas, visto que o resultado é uma imagem completamente distorcida e descaracterizada. Por outro lado, o ORB foi capaz de encontrar uma transformação mais satisfatória, com parte da imagem sendo perdida devido à translação vertical.

  \begin{figure}[!htbp]
    \centering
    \quad
    \subfloat[]{
      \label{fig:sift-1-warped}
      \includegraphics[width=.9\textwidth]{./images/photo1-sift-warped.jpg}
    } \\
    \quad
    \subfloat[]{
      \label{fig:sift-5-warped}
      \includegraphics[width=.9\textwidth]{./images/photo5-sift-warped.jpg}
    } \\
    \caption{Imagem com conteúdo à direita após transformação perspectiva para o espaço de coordeandas da imagem à esquerda, usando as correspondências encontradas pelo SIFT.}
    \label{fig:sift-warped}
  \end{figure}

  \begin{figure}[!htbp]
    \centering
    \quad
    \subfloat[]{
      \label{fig:orb-1-warped}
      \includegraphics[width=.9\textwidth]{./images/photo1-orb-warped.jpg}
    } \\
    \quad
    \subfloat[]{
      \label{fig:orb-5-warped}
      \includegraphics[width=.9\textwidth]{./images/photo5-orb-warped.jpg}
    } \\
    \caption{Imagem com conteúdo à direita após transformação perspectiva para o espaço de coordeandas da imagem à esquerda, usando as correspondências encontradas pelo ORB.}
    \label{fig:orb-warped}
  \end{figure}

  \subsection{Image Panorâmica}
    Finalmente, assumindo que o conteúdo da imagem à direita tem pelo menos uma componente de translação para direita em relação ao conteúdo da imagem à esquerda, basta colocar a imagem da esquerda no canto esquerdo da imagem transformada.

    As Figuras~\ref{fig:sift-1-result} e ~\ref{fig:orb-1-result} mostram o resultado do costuramento das Figuras~\ref{fig:1a} e~\ref{fig:1b} usando o SIFT e ORB como algoritmos de detecção e descrição de pontos, respectivamente. Note como o SIFT apresentou resultados melhores, identificando a transformação de escala apropriada para a imagem à direita. Nas Figuras~\ref{fig:sift-5-result} e~\ref{fig:orb-5-result}, temos o resultado do costuramento das Figuras~\ref{fig:5a} e~\ref{fig:5b}, onde nota-se que o SIFT obteve resultados insatisfatórios devido à identificação de correspondências equivocadas e o ORB apresentou resultados coerentes com o esperado, estendendo a imagem da esquerda só um pouco para direita, já que o par de imagens tem uma intersecção muito grande. Note que seria possível obter resultados satisfatórios com ambos os algoritmos, se os argumentos fossem modificados apropriadamente para o conjunto de imagens de entrada. Entretanto, achamos interessante ilustrar como não existe um algoritmo objetivamente melhor e que a escolha deve ser feita empiricamente, avaliando o resultado de cada um nas imagens de entrada.

  \begin{figure}[!htbp]
    \centering
    \quad
    \subfloat[]{
      \label{fig:sift-1-result}
      \includegraphics[width=.9\textwidth]{./images/photo1-sift-result.jpg}
    } \\
    \quad
    \subfloat[]{
      \label{fig:sift-5-result}
      \includegraphics[width=.9\textwidth]{./images/photo5-sift-result.jpg}
    } \\
    \caption{Imagem panorâmica no espaço de coordenadas da imagem com conteúdo mais à esquerda obtida por meio do algoritmo SIFT.}
    \label{fig:sift-result}
  \end{figure}

  \begin{figure}[!htbp]
    \centering
    \quad
    \subfloat[]{
      \label{fig:orb-1-result}
      \includegraphics[width=.9\textwidth]{./images/photo1-orb-result.jpg}
    } \\
    \quad
    \subfloat[]{
      \label{fig:orb-5-result}
      \includegraphics[width=.9\textwidth]{./images/photo5-orb-result.jpg}
    } \\
    \caption{Imagem panorâmica no espaço de coordenadas da imagem com conteúdo mais à esquerda obtida por meio do algoritmo ORB.}
    \label{fig:orb-result}
  \end{figure}
