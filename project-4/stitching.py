import os
import re
import glob
import argparse

import cv2
import numpy as np

def display_images(images):

    # Find max height and compute total width
    total_width = 0
    max_height = 0

    for img in images:
        total_width += img.shape[1]
        max_height = max(max_height, img.shape[0])

    # Determine if images are colored or grayscale
    if len(images[0].shape) > 2:
        shape = (max_height, total_width, 3)
    else:
        shape = (max_height, total_width)

    # Stitch images into a huge panel
    stitched = np.zeros(shape=shape, dtype=np.uint8)
    cur_width = 0
    for img in images:
        stitched[:img.shape[0], cur_width:cur_width + img.shape[1]] = img
        cur_width += img.shape[1]

    cv2.imshow("Stitched", stitched)
    cv2.waitKey(0)

def get_matches(descriptors1, descriptors2, feat_type):
    if feat_type == 'orb':
        # Needs to be uint8 to compute Hamming norm
        descriptors1 = descriptors1.astype(np.uint8)
        descriptors2 = descriptors2.astype(np.uint8)

        # Hamming norm is more appropriate for orb and brief descriptors
        normType = cv2.NORM_HAMMING
    elif feat_type == 'sift' or feat_type == 'surf':
        # According to Lowe's paper, L2 norm is more appropriate for sift
        normType = cv2.NORM_L2
    else:
        raise NotImplementedError

    # Create matcher object that only selects matches if they are the best one
    # for each other in both query and train datasets
    matcher = cv2.BFMatcher(normType=normType, crossCheck=True)
    all_matches = matcher.match(descriptors1, descriptors2)

    # Sort matches and keep only the best ones. Could be susceptible to noise of
    # background features
    good_matches = sorted(all_matches, key=lambda x:x.distance)[:20]

    return all_matches, good_matches

def get_features(img1, img2, feat_type):

    # Select feature detector algorithm
    nfeatures = 200
    if feat_type == 'orb':
        detector = cv2.ORB_create(nfeatures=nfeatures)
    elif feat_type == 'sift':
        detector = cv2.xfeatures2d.SIFT_create(nfeatures=nfeatures)
    elif feat_type == 'surf':
        detector = cv2.xfeatures2d.SURF_create()
    else:
        raise NotImplementedError

    kp1, des1 = detector.detectAndCompute(img1, mask=None)
    kp2, des2 = detector.detectAndCompute(img2, mask=None)

    return kp1, des1, kp2, des2

def stitch_images(images, name, output_dir, ext, feat_type, show):

    save_path = lambda fn: os.path.join(output_dir, fn)

    # For now we only know how to stitch two images
    images = images[:2]
    img1, img2 = images

    h1, w1, _ = img1.shape
    h2, w2, _ = img2.shape

    # Convert all images to grayscale
    gray1, gray2 = tuple(map(lambda img: cv2.cvtColor(img, cv2.COLOR_BGR2GRAY), images))

    # Get keypoints and descriptors
    kp1, des1, kp2, des2 = get_features(gray1, gray2, feat_type=feat_type)

    # Draw keypoints for each image for visualization
    img1_kps = cv2.drawKeypoints(img1, kp1, outImage=None, color=(0, 255, 0))
    img2_kps = cv2.drawKeypoints(img2, kp2, outImage=None, color=(0, 255, 0))

    # Merge two images as one
    total_width = w1 + w2
    height = max(h1, h2)
    img_kps = np.zeros((height, total_width, 3), dtype=np.uint8)
    img_kps[:h1, :w1] = img1_kps
    img_kps[:h2, w1:] = img2_kps

    cv2.imwrite(save_path(f'{name}-{feat_type}-keypoints.{ext}'), img_kps)
    if show:
        cv2.imshow('Keypoints', img_kps)
        cv2.waitKey(0)

    # Find best matches for the given keypoint descriptors
    all_matches, good_matches = get_matches(des1, des2, feat_type=feat_type)

    # Draw all matches found by brute-force matcher
    img_all_matches = cv2.drawMatches(
        img1=img1,
        keypoints1=kp1,
        img2=img2,
        keypoints2=kp2,
        matches1to2=all_matches,
        matchColor=(0, 0, 255),
        singlePointColor=(0, 255, 0),
        outImg=None,
        flags=cv2.DRAW_MATCHES_FLAGS_DEFAULT
    )

    # Draw 20 best matches
    img_best_matches = cv2.drawMatches(
        img1=img1,
        keypoints1=kp1,
        img2=img2,
        keypoints2=kp2,
        matches1to2=good_matches,
        matchColor=(0, 0, 255),
        singlePointColor=(0, 255, 0),
        outImg=None,
        flags=cv2.DRAW_MATCHES_FLAGS_DEFAULT
    )

    # Draw 20 best matches
    img_only_best_matches = cv2.drawMatches(
        img1=img1,
        keypoints1=kp1,
        img2=img2,
        keypoints2=kp2,
        matches1to2=good_matches,
        matchColor=(0, 0, 255),
        singlePointColor=(0, 255, 0),
        outImg=None,
        flags=cv2.DRAW_MATCHES_FLAGS_NOT_DRAW_SINGLE_POINTS
    )

    cv2.imwrite(save_path(f'{name}-{feat_type}-all-matches.{ext}'), img_all_matches)
    cv2.imwrite(save_path(f'{name}-{feat_type}-best-matches.{ext}'), img_best_matches)
    cv2.imwrite(save_path(f'{name}-{feat_type}-only-best-matches.{ext}'), img_only_best_matches)

    if show:
        cv2.imshow("Matches", img_only_best_matches)
        cv2.waitKey(0)

    img1_pts = np.float32([kp1[m.queryIdx].pt for m in good_matches]).reshape(-1,1,2)
    img2_pts = np.float32([kp2[m.trainIdx].pt for m in good_matches]).reshape(-1,1,2)

    # Find homography matrix that takes points from image2 to image1
    M, mask = cv2.findHomography(img2_pts, img1_pts, cv2.RANSAC, 5.0)

    # Warp image1 to image2's space
    warped = cv2.warpPerspective(img2, M, dsize=(w1 + w2, h1))
    cv2.imwrite(save_path(f'{name}-{feat_type}-warped.{ext}'), warped)

    # Add image1 to the leftmost part of the warped image2
    warped[0:h1, 0:w1] = img1
    cv2.imwrite(save_path(f'{name}-{feat_type}-result.{ext}'), warped)

    if show:
        cv2.imshow("Warped", warped)
        cv2.waitKey(0)

def main(assets_dir, output_dir, ext, feat_type='orb', show=False):

    img_fns = glob.glob(os.path.join(assets_dir, 'photo*'))

    # Group filenames for images that should be stitched together
    grouped_fns = {}
    for img_fn in img_fns:
        img_id = int(re.findall('\d+', img_fn)[0])
        group = grouped_fns.get(img_id, [])
        group.append(img_fn)
        grouped_fns[img_id] = group

    for img_fns in grouped_fns.values():
        name = os.path.basename(img_fns[0]).split('.')[0]    # Ignore dirs and file extensions
        name = ''.join([c for c in name if not c.isupper()]) # Remove upper case chars
        img_fns = sorted(img_fns, reverse=True)
        imgs = [cv2.imread(img_fn, cv2.IMREAD_COLOR) for img_fn in img_fns]

        # Stitch two images together
        stitch_images(imgs, name, output_dir, ext, feat_type, show)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-o", "--output_dir", required=False, default='bin')
    parser.add_argument("-a", "--assets_dir", required=False, default='assets')
    parser.add_argument("-t", "--feat_type", required=False, default='orb')
    parser.add_argument("--ext", required=False, default='jpg')
    parser.add_argument("--show", action='store_true')

    args = parser.parse_args()
    kwargs = vars(args)

    try:
        main(**kwargs)
    finally:
        cv2.destroyAllWindows()
