import numpy as np

dither_1 = np.array([
    [6, 8, 4],
    [1, 0, 3],
    [5, 2, 7]
])

dither_bayer = np.array([
    [0, 12, 3, 15],
    [8, 4, 11, 7],
    [2, 14, 1, 13],
    [10, 6, 9, 5]
])
