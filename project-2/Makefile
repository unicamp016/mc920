# Info
NAME=rafael_figueiredo_prudencio
RA=186145

# Programs
PYTHON = python3.6

# Directories
OUT_DIR = bin
SRC_DIR = .
ASSETS_DIR = assets
REPORT_DIR = report

# Files
REPORT = main
MAIN = halftoning

.PHONY: run clean build write-report submit

submit: build
	@mkdir -p $(OUT_DIR)/code
	@rsync -a Makefile *.py $(REPORT_DIR) --exclude='**/__pycache__' $(OUT_DIR)/code
	@cd $(OUT_DIR); zip -r9 $(NAME)_$(RA) $(REPORT).pdf code
	@rm -rf $(OUT_DIR)/code

build: $(OUT_DIR)/$(REPORT).pdf

clean:
	@find . -name "*.pyc" -delete
	@rm -rf $(OUT_DIR)/*

run:
	@mkdir -p $(OUT_DIR)
	$(PYTHON) $(SRC_DIR)/$(MAIN).py -o $(OUT_DIR) -a $(ASSETS_DIR)

write-report: $(REPORT_DIR)/$(REPORT).tex
	latexmk -pdf -pvc -cd $< -aux-directory=$(OUT_DIR) -output-directory=$(OUT_DIR)

$(OUT_DIR)/$(REPORT).pdf: $(REPORT_DIR)/$(REPORT).tex
	latexmk -pdf -cd $< -aux-directory=$(OUT_DIR) -output-directory=$(OUT_DIR)
